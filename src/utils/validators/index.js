const isNotEmpty = value => value.trim().length > 0;

export { isNotEmpty };
