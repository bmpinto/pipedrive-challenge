import config from "../../sdk/config";

const person = {
  id: 100,
  company_id: 4800475,
  owner_id: {
    id: 7525505,
    name: "Mocked Name",
    email: "mocked@email.com",
    has_pic: false,
    pic_hash: null,
    active_flag: true,
    value: 7525505
  },
  org_id: {
    name: "Mocked Organization",
    people_count: 4,
    owner_id: 7525505,
    address: null,
    cc_email: "bmpinto-19dc91@pipedrivemail.com",
    value: 69
  },
  name: "Mocked Name",
  first_name: "Mocked",
  last_name: "Name",
  open_deals_count: 0,
  related_open_deals_count: 0,
  closed_deals_count: 0,
  related_closed_deals_count: 0,
  participant_open_deals_count: 0,
  participant_closed_deals_count: 0,
  email_messages_count: 0,
  activities_count: 0,
  done_activities_count: 0,
  undone_activities_count: 0,
  reference_activities_count: 0,
  files_count: 0,
  notes_count: 0,
  followers_count: 1,
  won_deals_count: 0,
  related_won_deals_count: 0,
  lost_deals_count: 0,
  related_lost_deals_count: 0,
  active_flag: true,
  phone: [
    {
      value: "1234567890",
      primary: true
    }
  ],
  email: [
    {
      value: "mocked@email.com",
      primary: true
    }
  ],
  first_char: "b",
  update_time: "2019-01-04 00:30:53",
  add_time: "2019-01-04 00:30:53",
  visible_to: "3",
  picture_id: {
    pictures: {
      128: "https://pipedrive-profile-pics.s3.amazonaws.com/2037b0c0150a2831bd969029d73efdd798b13b98_128.jpg",
      512: "https://pipedrive-profile-pics.s3.amazonaws.com/2037b0c0150a2831bd969029d73efdd798b13b98_128.jpg"
    }
  },
  next_activity_date: null,
  next_activity_time: null,
  next_activity_id: null,
  last_activity_id: null,
  last_activity_date: null,
  last_incoming_mail_time: null,
  last_outgoing_mail_time: null,
  label: null,
  f25510f49d67a938f44c41231844d77735db61ec: "Mocked Assistant Name",
  "119cd790c40b1dd5130c32027ebabbeb1198a94d": "Mocked Groups",
  "8d72ca83304671408de120515757a1b557ca7010": "Mocked Location",
  org_name: "Mocked Organization",
  cc_email: "bmpinto-19dc91@pipedrivemail.com",
  owner_name: "Bruno Pinto"
};

const mockedPerson = {
  data: { ...person }
};

const mockedPersons = {
  data: [person]
};

const mockedPersonToStandardBody = {
  [config.customFields.assistant]: "Mocked Assistant Name",
  [config.customFields.groups]: "Mocked Groups",
  [config.customFields.location]: "Mocked Location",
  email: "mocked@email.com",
  name: "Mocked Name",
  phone: "1234567890",
  org_id: 1
};

const mockAdaptedPerson = {
  assistant: "Mocked Assistant Name",
  groups: "Mocked Groups",
  location: "Mocked Location",
  email: "mocked@email.com",
  id: 100,
  name: "Mocked Name",
  organization: "Mocked Organization",
  phone: "1234567890",
  picture:
    "https://pipedrive-profile-pics.s3.amazonaws.com/2037b0c0150a2831bd969029d73efdd798b13b98_128.jpg"
};

const mockedAdaptedPersons = [mockAdaptedPerson];

export {
  mockedPersonToStandardBody,
  mockAdaptedPerson,
  mockedPerson,
  mockedPersons,
  mockedAdaptedPersons
};
