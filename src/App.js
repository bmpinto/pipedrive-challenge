import React from "react";

// components
import { Header, PersonsContainer, SkipLink } from "components";

// styles
import styles from "./App.module.scss";

const contentId = "mainContent";

const App = () => (
  <>
    <SkipLink className={styles.skipLink} to={contentId}>
      Skip to main content
    </SkipLink>

    <Header />

    <main id={contentId} className={styles.mainContent}>
      <PersonsContainer />
    </main>
  </>
);

export default App;
