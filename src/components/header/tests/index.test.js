import Header from "../Header";

describe("Export Header", () => {
  it("should export the Header component", () => {
    expect(Header).toBeDefined();
    expect(typeof Header).toBe("function");
  });
});
