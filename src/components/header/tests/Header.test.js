import React from "react";
import { shallow } from "enzyme";
import Header from "../Header";

const getShallowWrapper = (props = {}) => shallow(<Header {...props} />);

describe("<Header />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });
});
