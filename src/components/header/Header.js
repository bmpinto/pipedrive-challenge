import React from "react";
import { ContentWrapper } from "components/content-wrapper";
import styles from "./Header.module.scss";
import { ReactComponent as Logo } from "media/logo/pipedrive.svg";

const Header = () => {
  return (
    <header className={styles.header}>
      <ContentWrapper>
        <Logo />
      </ContentWrapper>
    </header>
  );
};

export default Header;
