import React from "react";
import { shallow } from "enzyme";
import ContentWrapper from "../ContentWrapper";

const baseProps = {
  children: <span>Custom text</span>
};

const getShallowWrapper = (props = {}) =>
  shallow(<ContentWrapper {...baseProps} {...props} />);

describe("<ContentWrapper />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
