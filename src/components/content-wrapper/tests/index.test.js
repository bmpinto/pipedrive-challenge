import ContentWrapper from "../ContentWrapper";

describe("Export ContentWrapper", () => {
  it("should export the ContentWrapper component", () => {
    expect(ContentWrapper).toBeDefined();
    expect(typeof ContentWrapper).toBe("function");
  });
});
