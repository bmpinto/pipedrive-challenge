import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// styles
import styles from "./ContentWrapper.module.scss";

const ContentWrapper = ({ className, children }) => {
  const wrapperStyles = classNames(styles.contentWrapper, className);

  return <div className={wrapperStyles}>{children}</div>;
};

ContentWrapper.propTypes = {
  children: PropTypes.node.isRequired
};

export default ContentWrapper;
