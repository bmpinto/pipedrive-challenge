import React from "react";
import { shallow } from "enzyme";
import isEqual from "lodash.isequal";
import sdk from "sdk";
import withPersonsController from "../../core/withPersonsController";

const mockedPerson = "Mocked Person 1";
const mockedPersons = ["Mocked Person 2", "Mocked Person 3"];

jest.mock("sdk", () => ({
  persons: {
    get: jest.fn(() => Promise.resolve({ data: mockedPersons })),
    add: jest.fn(() => Promise.resolve({ data: mockedPerson })),
    remove: jest.fn(() => Promise.resolve({ data: mockedPersons[0] }))
  }
}));

const getShallowWrapper = () => {
  const WrappedComponent = () => <div />;
  const WrapperComponent = withPersonsController(WrappedComponent);

  return shallow(<WrapperComponent />);
};

describe("<withFormValidations />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should call sdk's persons.get() and set persons in state", async () => {
    const wrapper = getShallowWrapper();

    wrapper.instance().handleGetPersons();

    expect(wrapper.state("isLoading")).toBeTruthy();
    expect(sdk.persons.get).toHaveBeenCalled();

    await sdk.persons.get();

    const areStatePersonsMockedPersons = isEqual(
      wrapper.state("persons"),
      mockedPersons
    );

    expect(areStatePersonsMockedPersons).toBeTruthy();
    expect(wrapper.state("isLoading")).toBeFalsy();
  });

  it("should call sdk's persons.add() and add a person to state", async () => {
    const wrapper = getShallowWrapper();

    wrapper.setState({ persons: mockedPersons });
    wrapper.instance().handleAddPerson(mockedPerson);

    expect(wrapper.state("isAdding")).toBeTruthy();
    expect(sdk.persons.add).toHaveBeenCalled();

    await sdk.persons.add(mockedPerson);

    const isPersonInState = wrapper
      .state("persons")
      .find(person => person === mockedPerson);

    expect(isPersonInState).toBeTruthy();
    expect(wrapper.state("isAdding")).toBeFalsy();
  });

  it("should callsdk's persons.remove() and remove a person from state", async () => {
    const mockedPerson = mockedPersons[0];
    const wrapper = getShallowWrapper();

    wrapper.setState({ persons: mockedPersons });
    wrapper.instance().handleRemovePerson(mockedPerson);

    expect(wrapper.state("isRemoving")).toBeTruthy();
    expect(sdk.persons.remove).toHaveBeenCalled();

    await sdk.persons.remove(mockedPerson);

    const isPersonInState = wrapper
      .state("persons")
      .find(person => person === mockedPerson);

    expect(isPersonInState).toBeFalsy();
    expect(wrapper.state("isRemoving")).toBeFalsy();
  });

  it("should set selected person", () => {
    const mockedPerson = mockedPersons[0];
    const wrapper = getShallowWrapper();

    wrapper.setState({ persons: mockedPersons });
    wrapper.instance().onSelectPerson(mockedPerson);

    expect(wrapper.state("selectedPerson")).toBe(mockedPerson);
  });
});
