import withPersonsController from "../withPersonsController";

describe("Export withPersonsController", () => {
  it("should export the withPersonsController component", () => {
    expect(withPersonsController).toBeDefined();
    expect(typeof withPersonsController).toBe("function");
  });
});
