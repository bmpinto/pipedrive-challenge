import React, { Component } from "react";

// utils
import sdk from "sdk";

const withPersonsController = WrappedComponent => {
  class PersonsController extends Component {
    constructor(props) {
      super(props);

      this.state = {
        isLoading: false,
        isAdding: false,
        isRemoving: false,
        selectedPerson: {},
        persons: []
      };

      this.personsControl = {
        actions: {
          getPersons: this.handleGetPersons,
          addPerson: this.handleAddPerson,
          removePerson: this.handleRemovePerson,
          onSelectPerson: this.onSelectPerson
        },
        state: {}
      };
    }

    render() {
      const personsControl = {
        ...this.personsControl,
        state: { ...this.personsControl.state, ...this.state }
      };

      return (
        <WrappedComponent {...this.props} personsControl={personsControl} />
      );
    }

    handleGetPersons = async () => {
      this.setState(() => ({ isLoading: true }));

      try {
        const result = await sdk.persons.get();

        this.setState(() => ({
          isLoading: false,
          persons: result.data
        }));
      } catch (e) {
        this.setState(() => ({ isLoading: false }));
      }
    };

    handleAddPerson = async person => {
      this.setState(() => ({ isAdding: true }));

      try {
        const result = await sdk.persons.add(person);

        this.setState(state => ({
          isAdding: false,
          persons: [result.data, ...state.persons]
        }));
      } catch (e) {
        this.setState(() => ({ isAdding: false }));
      }
    };

    handleRemovePerson = async id => {
      this.setState(() => ({ isRemoving: true }));

      try {
        const result = await sdk.persons.remove(id);

        this.setState(state => ({
          isRemoving: false,
          persons: state.persons.filter(person => person.id !== result.data.id)
        }));
      } catch (e) {
        this.setState(() => ({ isRemoving: false }));
      }
    };

    onSelectPerson = person =>
      this.setState(() => ({ selectedPerson: person }));
  }

  return PersonsController;
};

export default withPersonsController;
