import React from "react";
import { shallow } from "enzyme";
import { mockedAdaptedPersons, mockAdaptedPerson } from "utils/mocks";
import PersonsList from "../PersonsList";

const baseProps = {
  persons: mockedAdaptedPersons
};

const getMountWrapper = (props = {}) =>
  shallow(<PersonsList {...baseProps} {...props} />);

describe("<PersonsList />", () => {
  it("should render correctly", () => {
    expect(getMountWrapper()).toMatchSnapshot();
  });

  it("should call onSelectPerson", () => {
    const onSelectPerson = jest.fn();
    const wrapper = getMountWrapper({ onSelectPerson });

    wrapper.instance().handleSelectedPerson({ mockAdaptedPerson });

    expect(onSelectPerson).toHaveBeenCalled();
  });
});
