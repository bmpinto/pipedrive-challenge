import PersonsList from "../PersonsList";

describe("Export PersonsList", () => {
  it("should export the PersonsList component", () => {
    expect(PersonsList).toBeDefined();
    expect(typeof PersonsList).toBe("function");
  });
});
