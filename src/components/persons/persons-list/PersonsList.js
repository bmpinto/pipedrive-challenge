import React, { Component } from "react";

// utils
import PropTypes from "prop-types";

// components
import { Button, PersonSummary } from "components";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

// styles
import styles from "./PersonsList.module.scss";

class PersonsList extends Component {
  static propTypes = {
    persons: PropTypes.array.isRequired,
    onSelectPerson: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      persons: props.persons
    };
  }

  componentDidUpdate(prevProps) {
    const { persons } = this.props;

    if (prevProps.persons.length !== persons.length) {
      this.setState({ persons });
    }
  }

  render() {
    const { persons } = this.state;

    if (!persons) {
      return null;
    }

    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable">
          {provided => (
            <ul ref={provided.innerRef} className={styles.list}>
              {persons.map(this.renderPerson)}
              {provided.placeholder}
            </ul>
          )}
        </Droppable>
      </DragDropContext>
    );
  }

  onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    const persons = this.reorder(
      this.state.persons,
      result.source.index,
      result.destination.index
    );

    this.setState({
      persons
    });
  };

  reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  renderPerson = (person, index) => {
    return (
      <Draggable key={person.id} draggableId={person.id} index={index}>
        {provided => (
          <li
            ref={provided.innerRef}
            {...provided.draggableProps}
            className={styles.listItem}
            key={`person-${person.id}`}
          >
            <div className={styles.dragHandle} {...provided.dragHandleProps} />

            <Button
              className={styles.listItemButton}
              onClick={() => this.handleSelectedPerson(person)}
            >
              <PersonSummary person={person} avatarSize="medium" />
            </Button>
          </li>
        )}
      </Draggable>
    );
  };

  handleSelectedPerson = person => {
    const { onSelectPerson } = this.props;

    onSelectPerson && onSelectPerson(person);
  };
}

export default PersonsList;
