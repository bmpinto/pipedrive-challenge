import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// components
import { PersonAvatar } from "../person-avatar";
import { avatarSizes } from "../person-avatar/PersonAvatar";

// styles
import styles from "./PersonSummary.module.scss";
import { IconOrganization } from "components/icons";

const PersonSummary = ({ avatarSize, person, theme }) => {
  return (
    <React.Fragment>
      <div className={classNames(theme.container)}>
        <div className={classNames(styles.name, theme.name)}>{person.name}</div>

        {person.organization && (
          <div className={classNames(styles.organization, theme.organization)}>
            <IconOrganization className={classNames(styles.icon, theme.icon)} />
            {person.organization}
          </div>
        )}
      </div>

      <PersonAvatar
        size={avatarSize}
        person={person}
        className={classNames(theme.avatar)}
      />
    </React.Fragment>
  );
};

PersonSummary.defaultProps = {
  theme: {}
};

PersonSummary.propTypes = {
  person: PropTypes.object.isRequired,
  theme: PropTypes.shape({
    container: PropTypes.string,
    name: PropTypes.string,
    organization: PropTypes.string,
    avatar: PropTypes.string
  }),
  avatarSize: PropTypes.oneOf([...Object.keys(avatarSizes)])
};

export default PersonSummary;
