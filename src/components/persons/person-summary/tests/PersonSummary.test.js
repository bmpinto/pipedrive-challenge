import React from "react";
import { mount } from "enzyme";
import { mockAdaptedPerson } from "utils/mocks";
import PersonSummary from "../PersonSummary";

const baseProps = {
  person: mockAdaptedPerson
};

const getMountWrapper = (props = {}) =>
  mount(<PersonSummary {...baseProps} {...props} />);

describe("<PersonSummary />", () => {
  it("should render correctly", () => {
    expect(getMountWrapper()).toMatchSnapshot();
  });
});
