import PersonSummary from "../PersonSummary";

describe("Export PersonSummary", () => {
  it("should export the PersonSummary component", () => {
    expect(PersonSummary).toBeDefined();
    expect(typeof PersonSummary).toBe("function");
  });
});
