import React, { Component } from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// styles
import styles from "./PersonAvatar.module.scss";

export const avatarSizes = {
  small: "small",
  medium: "medium",
  large: "large"
};

class PersonAvatar extends Component {
  static propTypes = {
    person: PropTypes.object.isRequired,
    size: PropTypes.oneOf([...Object.values(avatarSizes)]),
    className: PropTypes.string
  };

  static defaultProps = {
    size: avatarSizes.small
  };

  render() {
    const {
      className,
      size,
      person: { picture }
    } = this.props;

    const avatarStyles = classNames(
      styles.itemAvatar,
      { [styles.itemAvatar__medium]: size === avatarSizes.medium },
      { [styles.itemAvatar__large]: size === avatarSizes.large },
      className
    );

    return (
      <div className={avatarStyles}>
        {picture ? this.renderPicture() : this.renderPlaceholder()}
      </div>
    );
  }

  renderPicture = () => {
    const {
      person: { name, picture }
    } = this.props;

    return (
      <img
        className={styles.picture}
        src={`${picture}`}
        alt={`${name} Photography`}
      />
    );
  };

  renderPlaceholder = () => {
    const {
      person: { name }
    } = this.props;

    const nameInitials =
      name &&
      name
        .split(" ")
        .map(el => el.charAt(0))
        .join("");

    return <span className={styles.placeholder}>{nameInitials}</span>;
  };
}

export default PersonAvatar;
