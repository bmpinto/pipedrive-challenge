import React from "react";
import { shallow } from "enzyme";
import { mockAdaptedPerson } from "utils/mocks";
import PersonAvatar from "../PersonAvatar";

const baseProps = {
  person: mockAdaptedPerson
};

const getShallowWrapper = (props = {}) =>
  shallow(<PersonAvatar {...baseProps} {...props} />);

describe("<PersonAvatar />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName",
      size: "medium"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });

  it("should render picture instead of name initials", () => {
    const wrapper = getShallowWrapper();

    const picture = wrapper.find(".picture");

    expect(picture).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });

  it("should render name initials instead of picture", () => {
    const mockedPerson = mockAdaptedPerson;
    delete mockedPerson.picture;

    const wrapper = getShallowWrapper({
      person: mockedPerson
    });

    const nameInitials = wrapper.find(".placeholder");

    expect(nameInitials).toHaveLength(1);
    expect(nameInitials.text()).toBe("MN");
  });
});
