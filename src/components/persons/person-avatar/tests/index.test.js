import PersonAvatar from "../PersonAvatar";

describe("Export PersonAvatar", () => {
  it("should export the PersonAvatar component", () => {
    expect(PersonAvatar).toBeDefined();
    expect(typeof PersonAvatar).toBe("function");
  });
});
