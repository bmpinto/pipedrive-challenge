import React from "react";
import { mount } from "enzyme";
import PersonForm from "../PersonForm";

const baseProps = {
  formName: "customForm",
  onBlur: jest.fn(),
  onSubmit: jest.fn(),
  onChange: jest.fn(),
  submitRef: React.createRef()
};

const getMountWrapper = (props = {}) =>
  mount(<PersonForm {...baseProps} {...props} />);

describe("<PersonForm />", () => {
  it("should render correctly", () => {
    expect(getMountWrapper()).toMatchSnapshot();
  });

  it("should call onSubmit", () => {
    const wrapper = getMountWrapper();

    wrapper.find("button[type='submit']").simulate("submit");

    expect(baseProps.onSubmit).toHaveBeenCalled();
  });
});
