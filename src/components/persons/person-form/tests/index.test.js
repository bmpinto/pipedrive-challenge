import PersonForm from "../PersonForm";

describe("Export PersonForm", () => {
  it("should export the PersonForm component", () => {
    expect(PersonForm).toBeDefined();
    expect(typeof PersonForm).toBe("function");
  });
});
