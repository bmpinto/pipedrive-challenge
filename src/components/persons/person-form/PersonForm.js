import React, { Component } from "react";

// utils
import PropTypes from "prop-types";
import { isNotEmpty } from "utils/validators";

// components
import { Form, FormField, withFormValidations } from "components/form";

// styles
import styles from "./PersonForm.module.scss";

class PersonForm extends Component {
  static propTypes = {
    errors: PropTypes.object,
    formName: PropTypes.string.isRequired,
    submitRef: PropTypes.object.isRequired,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  };

  static defaultProps = {
    errors: {}
  };

  render() {
    const {
      errors,
      formName,
      onBlur,
      onChange,
      onSubmit,
      submitRef
    } = this.props;
    const commonFieldProps = { formName, onChange, onBlur };

    return (
      <div className={styles.container}>
        <Form onSubmit={onSubmit} submitRef={submitRef}>
          <FormField
            name="name"
            label="Name"
            error={errors.name}
            required
            {...commonFieldProps}
          />

          <FormField
            name="email"
            label="Email"
            type="email"
            error={errors.email}
            {...commonFieldProps}
          />

          <FormField
            name="phone"
            label="Phone"
            error={errors.phone}
            {...commonFieldProps}
          />

          <FormField
            name="assistant"
            label="Assistant Name"
            error={errors.phone}
            {...commonFieldProps}
          />

          <FormField
            name="groups"
            label="Groups"
            error={errors.groups}
            {...commonFieldProps}
          />

          <FormField
            name="location"
            label="Location"
            error={errors.location}
            {...commonFieldProps}
          />
        </Form>
      </div>
    );
  }
}

const validators = {
  name: [isNotEmpty, "Name should not be  empty."]
};

export default withFormValidations({ validators })(PersonForm);
