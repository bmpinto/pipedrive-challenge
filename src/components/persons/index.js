import { PersonAvatar } from "./person-avatar";
import { PersonDetails } from "./person-details";
import { PersonForm } from "./person-form";
import { PersonsActions } from "./persons-actions";
import { PersonsContainer } from "./persons-container";
import { PersonsList } from "./persons-list";
import { PersonSummary } from "./person-summary";
import { withPersonsController } from "./core";

export {
  PersonAvatar,
  PersonDetails,
  PersonForm,
  PersonsActions,
  PersonsContainer,
  PersonsList,
  PersonSummary,
  withPersonsController
};
