import React, { Component } from "react";

// utils
import PropTypes from "prop-types";

// components
import { PersonAvatar, VisuallyHidden } from "components";

// styles
import styles from "./PersonDetails.module.scss";

class PersonDetails extends Component {
  static propTypes = {
    person: PropTypes.object.isRequired
  };

  render() {
    return (
      <div className={styles.container}>
        {this.renderHeading()}
        {this.renderDetails()}
      </div>
    );
  }

  renderHeading = () => {
    const { person } = this.props;

    return (
      <div className={styles.personHeading}>
        <PersonAvatar person={person} size="large" />

        <p className={styles.personName}>{person.name}</p>

        {person.phone && (
          <a className={styles.personPhone} href={`tel:${person.phone}`}>
            <VisuallyHidden>Phone number:</VisuallyHidden>
            {person.phone}
          </a>
        )}
      </div>
    );
  };

  renderDetails = () => {
    const { person } = this.props;

    return (
      <div className={styles.personDetails}>
        <dl className={styles.definitionList}>
          {person.email && (
            <>
              <dt className={styles.term}>email</dt>
              <dd className={styles.termDefinition}>
                <a href={`mailto:${person.email}`}>{person.email}</a>
              </dd>
            </>
          )}
          {person.organization && (
            <>
              <dt className={styles.term}>organization</dt>
              <dd className={styles.termDefinition}>{person.organization}</dd>
            </>
          )}
          {person.assistant && (
            <>
              <dt className={styles.term}>assistant</dt>
              <dd className={styles.termDefinition}>{person.assistant}</dd>
            </>
          )}
          {person.groups && (
            <>
              <dt className={styles.term}>groups</dt>
              <dd className={styles.termDefinition}>{person.groups}</dd>
            </>
          )}
          {person.location && (
            <>
              <dt className={styles.term}>location</dt>
              <dd className={styles.termDefinition}>{person.location}</dd>
            </>
          )}
        </dl>
      </div>
    );
  };
}

export default PersonDetails;
