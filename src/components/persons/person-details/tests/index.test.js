import PersonDetails from "../PersonDetails";

describe("Export PersonDetails", () => {
  it("should export the PersonDetails component", () => {
    expect(PersonDetails).toBeDefined();
    expect(typeof PersonDetails).toBe("function");
  });
});
