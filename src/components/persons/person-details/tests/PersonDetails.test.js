import React from "react";
import { shallow } from "enzyme";
import { mockAdaptedPerson } from "utils/mocks";
import PersonDetails from "../PersonDetails";

const baseProps = {
  person: mockAdaptedPerson
};

const getShallowWrapper = (props = {}) =>
  shallow(<PersonDetails {...baseProps} {...props} />);

describe("<PersonDetails />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });
});
