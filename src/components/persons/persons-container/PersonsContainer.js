import React, { Component } from "react";
import PropTypes from "prop-types";

import { withPersonsController } from "../core";

// components
import {
  Button,
  ContentWrapper,
  Loader,
  PersonsActions,
  PersonsList,
  Modal,
  ModalHeader,
  ModalContent,
  ModalActions,
  PersonDetails,
  PersonForm
} from "components";

// styles
import styles from "./PersonsContainer.module.scss";

export class PersonsContainer extends Component {
  static propTypes = {
    personsControl: PropTypes.shape({
      actions: PropTypes.object.isRequired,
      state: PropTypes.object.isRequired
    })
  };

  constructor(props) {
    super(props);

    this.state = {
      isPersonDetailsOpen: false,
      isPersonFormOpen: false
    };

    this.submitPersonFormRef = React.createRef();
  }

  componentDidMount() {
    this.props.personsControl.actions.getPersons();
  }

  render() {
    const { isPersonDetailsOpen, isPersonFormOpen } = this.state;

    const {
      state: { isLoading, persons }
    } = this.props.personsControl;

    return (
      <>
        <PersonsActions
          onSelectPerson={this.handleSelectedPerson}
          onClickAddPerson={this.handlePersonFormOpen}
          persons={persons}
        />
        <ContentWrapper>
          {isLoading && <Loader className={styles.loader} />}
          {!isLoading && (
            <PersonsList
              persons={persons}
              onSelectPerson={this.handleSelectedPerson}
            />
          )}
        </ContentWrapper>

        {isPersonDetailsOpen && this.renderPersonDetails()}
        {isPersonFormOpen && this.renderAddPersonForm()}
      </>
    );
  }

  renderAddPersonForm = () => {
    const { isPersonFormOpen } = this.state;
    const {
      state: { isAddingPerson }
    } = this.props.personsControl;

    return (
      <Modal
        isActive={isPersonFormOpen}
        onDismiss={this.handlePersonFormDismiss}
      >
        <ModalHeader
          title="Add new person"
          onCloseButtonClick={this.handlePersonFormDismiss}
        />
        <ModalContent>
          <PersonForm
            formName="personForm"
            submitRef={this.submitPersonFormRef}
            onSubmit={this.handleAddPerson}
          />
        </ModalContent>
        <ModalActions
          className={styles.actionsContainer}
          onCloseButtonClick={this.handlePersonFormDismiss}
        >
          <Button
            variant="success"
            isLoading={isAddingPerson}
            onClick={() => this.submitPersonFormRef.current.click()}
          >
            Save
          </Button>
        </ModalActions>
      </Modal>
    );
  };

  renderPersonDetails = () => {
    const { isPersonDetailsOpen } = this.state;

    const {
      state: { isRemovingPerson, selectedPerson }
    } = this.props.personsControl;

    return (
      <Modal
        isActive={isPersonDetailsOpen}
        onDismiss={this.handleDetailsDismiss}
      >
        <ModalHeader
          title="Personal information"
          onCloseButtonClick={this.handleDetailsDismiss}
        />
        <ModalContent>
          <PersonDetails person={selectedPerson} />
        </ModalContent>
        <ModalActions
          className={styles.actionsContainer}
          onCloseButtonClick={this.handleDetailsDismiss}
        >
          <Button
            variant="danger"
            isLoading={isRemovingPerson}
            onClick={() => this.handleRemovePerson(selectedPerson)}
          >
            Delete
          </Button>
        </ModalActions>
      </Modal>
    );
  };

  handleAddPerson = person => {
    const {
      actions: { addPerson }
    } = this.props.personsControl;

    addPerson(person).then(() => this.handlePersonFormDismiss());
  };

  handleRemovePerson = person => {
    const {
      actions: { removePerson }
    } = this.props.personsControl;

    removePerson(person.id).then(() => this.handleDetailsDismiss());
  };

  handleSelectedPerson = person => {
    const {
      actions: { onSelectPerson }
    } = this.props.personsControl;

    onSelectPerson(person);
    this.handleDetailsOpen();
  };

  handlePersonFormDismiss = () => this.setState({ isPersonFormOpen: false });

  handlePersonFormOpen = () => this.setState({ isPersonFormOpen: true });

  handleDetailsDismiss = () => this.setState({ isPersonDetailsOpen: false });

  handleDetailsOpen = () =>
    this.setState(() => ({ isPersonDetailsOpen: true }));
}

export default withPersonsController(PersonsContainer);
