import React from "react";
import { shallow, mount } from "enzyme";
import { PersonsContainer } from "../PersonsContainer";
import ReactModal from "react-modal";

const baseProps = {
  personsControl: {
    actions: {
      getPersons: () => {},
      addPerson: () => {},
      removePerson: () => {},
      onSelectPerson: () => {}
    },
    state: {
      isLoading: false,
      isAddingPerson: false,
      isRemovingPerson: false,
      selectedPerson: {},
      persons: []
    }
  }
};

const getShallowWrapper = (props = {}) =>
  shallow(<PersonsContainer {...baseProps} {...props} />);

const getMountWrapper = (props = {}) =>
  mount(<PersonsContainer {...baseProps} {...props} />);

describe("<PersonsContainer />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render PersonDetails", () => {
    ReactModal.setAppElement("body");
    const wrapper = getMountWrapper();

    const isPersonDetailsRendered = wrapper
      .setState({ isPersonDetailsOpen: true })
      .update()
      .find("PersonDetails");

    expect(isPersonDetailsRendered).toBeTruthy();
  });

  it("should render PersonForm", () => {
    ReactModal.setAppElement("body");
    const wrapper = getMountWrapper();

    const isPersonFormRendered = wrapper
      .setState({ isPersonFormOpen: true })
      .update()
      .find("PersonForm");

    expect(isPersonFormRendered).toBeTruthy();
  });
});
