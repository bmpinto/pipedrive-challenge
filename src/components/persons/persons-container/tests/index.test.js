import PersonsContainer from "../PersonsContainer";

describe("Export PersonsContainer", () => {
  it("should export the PersonsContainer component", () => {
    expect(PersonsContainer).toBeDefined();
    expect(typeof PersonsContainer).toBe("function");
  });
});
