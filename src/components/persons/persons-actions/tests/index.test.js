import PersonsActions from "../PersonsActions";

describe("Export PersonsActions", () => {
  it("should export the PersonsActions component", () => {
    expect(PersonsActions).toBeDefined();
    expect(typeof PersonsActions).toBe("function");
  });
});
