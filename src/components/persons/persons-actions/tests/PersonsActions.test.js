import React from "react";
import { shallow } from "enzyme";
import { mockedAdaptedPersons, mockAdaptedPerson } from "utils/mocks";
import PersonsActions from "../PersonsActions";

const baseProps = {
  persons: mockedAdaptedPersons
};

const getMountWrapper = (props = {}) =>
  shallow(<PersonsActions {...baseProps} {...props} />);

describe("<PersonsActions />", () => {
  it("should render correctly", () => {
    expect(getMountWrapper()).toMatchSnapshot();
  });

  it("should call onClickAddPerson", () => {
    const onClickAddPerson = jest.fn();
    const wrapper = getMountWrapper({ onClickAddPerson });

    wrapper.instance().handleAddPerson();

    expect(onClickAddPerson).toHaveBeenCalled();
  });

  it("should call onSelectPerson", () => {
    const onSelectPerson = jest.fn();
    const wrapper = getMountWrapper({ onSelectPerson });

    wrapper.instance().onSuggestionSelected(null, { mockAdaptedPerson });

    expect(onSelectPerson).toHaveBeenCalled();
  });

  it("should find people with mocked name", () => {
    const wrapper = getMountWrapper();
    const mockedName = "mOcKed";

    const filteredPersons = wrapper.instance().getSuggestions(mockedName);

    expect(filteredPersons).not.toHaveLength(0);
  });

  it("should not find people with mocked name", () => {
    const wrapper = getMountWrapper();
    const mockedName = "nOtmOcKed";

    const filteredPersons = wrapper.instance().getSuggestions(mockedName);

    expect(filteredPersons).toHaveLength(0);
  });
});
