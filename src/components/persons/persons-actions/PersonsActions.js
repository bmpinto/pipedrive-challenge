import React, { Component } from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// components
import AutoSuggest from "react-autosuggest";
import { Button, ContentWrapper, PersonSummary } from "components";

// styles
import styles from "./PersonsActions.module.scss";
import { IconSearch } from "components/icons";

class PersonsActions extends Component {
  static propTypes = {
    persons: PropTypes.array.isRequired,
    onSelectPerson: PropTypes.func,
    onClickAddPerson: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      value: "",
      suggestions: props.persons
    };
  }

  render() {
    return (
      <div className={styles.borderedContainer}>
        <ContentWrapper className={styles.heading}>
          <h1 className={styles.title}>People's list</h1>

          <div className={styles.actionsWrapper}>
            {this.renderSearchPerson()}
            {this.renderAddPerson()}
          </div>
        </ContentWrapper>
      </div>
    );
  }

  renderAddPerson = () => (
    <Button variant="success" onClick={this.handleAddPerson}>
      Add person
    </Button>
  );

  handleAddPerson = () => {
    const { onClickAddPerson } = this.props;

    onClickAddPerson && onClickAddPerson();
  };

  renderSearchPerson = () => {
    const { value, suggestions } = this.state;

    const inputProps = {
      placeholder: "Search for people",
      value,
      onChange: this.onChange
    };

    const autoSuggestStyles = {
      input: styles.input,
      suggestionsContainer: styles.suggestionsContainer,
      suggestionsContainerOpen: styles.suggestionsContainerOpen,
      suggestionsList: styles.suggestionsList,
      suggestion: styles.suggestion,
      suggestionHighlighted: styles.suggestionHighlighted
    };

    return (
      <div className={styles.inputWrapper}>
        <AutoSuggest
          getSuggestionValue={this.getSuggestionValue}
          highlightFirstSuggestion
          inputProps={inputProps}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          onSuggestionSelected={this.onSuggestionSelected}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          renderSuggestion={this.renderSuggestion}
          suggestions={suggestions}
          theme={autoSuggestStyles}
          renderSuggestionsContainer={this.renderSuggestionsContainer}
        />

        <span className={classNames(styles.button, styles.searchButton)}>
          <IconSearch className={styles.icon} />
        </span>
      </div>
    );
  };

  onChange = (_, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  getSuggestionValue = () => this.state.value;

  onSuggestionsClearRequested = () => {
    this.setState(() => ({
      suggestions: []
    }));
  };

  onSuggestionSelected = (_, { suggestion }) => {
    const { onSelectPerson } = this.props;

    onSelectPerson && onSelectPerson({ ...suggestion });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  getSuggestions = value => {
    const { persons } = this.props;
    const inputValue = value.trim().toLowerCase();

    return inputValue.length === 0
      ? []
      : persons.filter(
          person => person.name.toLowerCase().indexOf(inputValue) !== -1
        );
  };

  renderSuggestion = person => (
    <PersonSummary
      person={person}
      theme={{
        container: styles.personSummaryContainer,
        name: styles.personSummaryName,
        icon: styles.personSummaryIcon,
        avatar: styles.personSummaryAvatar,
        organization: styles.personSummaryOrganization
      }}
    />
  );
}

export default PersonsActions;
