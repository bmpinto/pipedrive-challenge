import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// styles
import styles from "./Loader.module.scss";

const Loader = ({ className, size }) => {
  const innerStyles = classNames(styles.loader, { [styles[`${size}`]]: size });

  return (
    <span className={className}>
      <span className={innerStyles} />
    </span>
  );
};

Loader.defaultProps = {
  size: "medium"
};

Loader.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium"])
};

export default Loader;
