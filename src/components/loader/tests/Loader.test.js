import React from "react";
import { shallow } from "enzyme";
import Loader from "../Loader";

const getShallowWrapper = (props = {}) => shallow(<Loader {...props} />);

describe("<Loader />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      size: "medium",
      className: "customClassName"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
