import Loader from "../Loader";

describe("Export Loader", () => {
  it("should export the Loader component", () => {
    expect(Loader).toBeDefined();
    expect(typeof Loader).toBe("function");
  });
});
