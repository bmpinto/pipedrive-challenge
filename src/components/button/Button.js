import React, { Component } from "react";

// utils
import classNames from "classnames";
import PropTypes from "prop-types";

// styles
import styles from "./Button.module.scss";

class Button extends Component {
  static propTypes = {
    children: PropTypes.node,
    isLoading: PropTypes.bool,
    className: PropTypes.string,
    variant: PropTypes.oneOf(["success", "danger"])
  };

  static defaultProps = {
    type: "button",
    isLoading: false
  };

  render() {
    const {
      children,
      isLoading,
      onClick,
      className,
      type,
      variant,
      ...domProps
    } = this.props;

    const buttonStyles = classNames(
      styles.button,
      {
        [styles[`${variant}`]]: variant,
        [styles.isLoading]: isLoading
      },
      className
    );

    const loadingProps = isLoading && { "aria-busy": true, tabIndex: -1 };

    return (
      <button
        className={buttonStyles}
        type={type}
        onClick={onClick}
        {...loadingProps}
        {...domProps}
      >
        {children}
      </button>
    );
  }
}

export default Button;
