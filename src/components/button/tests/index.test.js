import Button from "../Button";

describe("Export Button", () => {
  it("should export the Button component", () => {
    expect(Button).toBeDefined();
    expect(typeof Button).toBe("function");
  });
});
