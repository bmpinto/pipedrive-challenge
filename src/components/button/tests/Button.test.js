import React from "react";
import { shallow } from "enzyme";
import Button from "../Button";

const getShallowWrapper = (props = {}) => shallow(<Button {...props} />);

describe("<Button />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      isLoading: true,
      variant: "success",
      children: <span>Custom text</span>,
      className: "customClassName"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
