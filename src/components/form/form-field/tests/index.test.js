import FormField from "../FormField";

describe("Export FormField", () => {
  it("should export the FormField component", () => {
    expect(FormField).toBeDefined();
    expect(typeof FormField).toBe("function");
  });
});
