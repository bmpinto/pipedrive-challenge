import React from "react";
import { shallow } from "enzyme";
import FormField from "../FormField";

const baseProps = {
  formName: "form",
  name: "name",
  label: "label"
};

const getShallowWrapper = (props = {}) =>
  shallow(<FormField {...baseProps} {...props} />);

describe("<FormField />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render with error props", () => {
    const wrapper = getShallowWrapper({ error: "error message" });
    const errorId = `#${baseProps.formName}-${baseProps.name}-error`;

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(errorId)).toHaveLength(1);
  });

  it("should render with custom props", () => {
    const customProps = {
      theme: { input: "inputClassName" },
      type: "email",
      id: "id"
    };

    const wrapper = getShallowWrapper({ ...customProps });

    expect(wrapper).toMatchSnapshot();
  });

  describe("Custom events", () => {
    it("should call onBlur", () => {
      const onBlur = jest.fn();
      const wrapper = getShallowWrapper({ onBlur });
      const button = wrapper.find("input");

      button.simulate("blur");

      expect(onBlur).toHaveBeenCalled();
    });

    it("should call onChange", () => {
      const onChange = jest.fn();
      const wrapper = getShallowWrapper({ onChange });
      const button = wrapper.find("input");

      button.simulate("change");

      expect(onChange).toHaveBeenCalled();
    });
  });
});
