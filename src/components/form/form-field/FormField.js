import React from "react";

import PropTypes from "prop-types";
import classNames from "classnames";

import styles from "./FormField.module.scss";

const FormField = ({
  error,
  label,
  formName,
  name,
  onBlur,
  onChange,
  theme,
  ...domProps
}) => {
  const hasError = !!error;
  const fieldIdentifier = `${formName}-${name}`;
  const errorProps = hasError && {
    "aria-invalid": true,
    "aria-describedby": `${fieldIdentifier}-error`
  };

  return (
    <div className={classNames(styles.inputContainer, theme.container)}>
      <label
        className={classNames(styles.label, theme.label)}
        htmlFor={fieldIdentifier}
      >
        {label}
      </label>

      <input
        onChange={e => onChange && onChange(e)}
        onBlur={e => onBlur && onBlur(e)}
        name={name}
        className={classNames(styles.input, theme.input)}
        id={fieldIdentifier}
        {...errorProps}
        {...domProps}
      />

      {hasError && (
        <span
          id={`${fieldIdentifier}-error`}
          className={classNames(styles.error, theme.error)}
        >
          {error}
        </span>
      )}
    </div>
  );
};

FormField.defaultProps = {
  type: "text",
  theme: {}
};

FormField.propTypes = {
  error: PropTypes.string,
  label: PropTypes.string.isRequired,
  formName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  theme: PropTypes.shape({
    container: PropTypes.string,
    input: PropTypes.string,
    error: PropTypes.string
  }),
  type: PropTypes.string
};

export default FormField;
