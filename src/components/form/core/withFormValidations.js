import React, { Component } from "react";

// utils
import PropTypes from "prop-types";
import pickBy from "lodash.pickby";
import identity from "lodash.identity";

/*
 *   // validators shape example
 *   const validators = {
 *     name: [isNotEmpty, "Name should not be  empty."]
 *  };
 *
 */

const defaultInitialState = {
  fields: {},
  errors: {}
};

const withFormValidations = ({
  initialState = defaultInitialState,
  validators
}) => WrappedComponent => {
  class Form extends Component {
    state = { ...initialState };
    validators = { ...validators };

    render() {
      if (this.validators.length === 0) {
        return null;
      }

      const { fields, errors } = this.state;

      return (
        <WrappedComponent
          {...this.props}
          fields={fields}
          errors={errors}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onSubmit={this.onSubmit}
        />
      );
    }

    onChange = event => {
      const { name, value } = event.target;

      this.handleChange(name, value);
    };

    handleChange = (key, value) => {
      // when typing we remove any errors that field may have
      const { [key]: _, ...errorsState } = this.state.errors;

      this.setState(state => ({
        fields: { ...state.fields, [key]: value.trim() },
        errors: errorsState
      }));
    };

    onSubmit = () => {
      const { fields } = this.state;
      const { onSubmit } = this.props;

      const isFormValid = Object.entries(fields).every(([key, value]) =>
        this.handleValidation(key, value)
      );

      if (isFormValid) {
        // clean empty fields
        const result = pickBy(fields, identity);

        onSubmit && onSubmit(result);
      }
    };

    onBlur = event => {
      const { name, value } = event.target;

      this.handleValidation(name, value);
    };

    handleValidation = (key, value) => {
      const [isValid, message] = this.validate(key, value);

      if (!isValid) {
        this.setState(state => ({
          errors: { ...state.errors, [key]: message }
        }));
      }

      return isValid;
    };

    validate = (key, value) => {
      const fieldValidator = this.validators[key];

      if (!fieldValidator) {
        // since it has no validator it's valid by default
        return [true];
      }

      const [validator, message] = fieldValidator;
      const isValid = validator && validator(value);

      return [isValid, message];
    };
  }

  return Form;
};

withFormValidations.propTypes = {
  initialState: PropTypes.shape({
    fields: PropTypes.object,
    errors: PropTypes.object
  }),
  validators: PropTypes.object.isRequired
};

export default withFormValidations;
