import React from "react";
import { shallow } from "enzyme";
import withFormValidations from "../../core/withFormValidations";

const mockedValidator = jest.fn();
const mockedField = ["mockedField", "mockFieldValue"];
const mockedFieldValidationError = "error message";

const getShallowWrapper = (validator = mockedValidator) => {
  const WrappedComponent = () => <form />;
  const WrapperComponent = withFormValidations({
    validators: {
      mockedField: [validator, mockedFieldValidationError]
    }
  })(WrappedComponent);

  return shallow(<WrapperComponent />);
};

describe("<withFormValidations />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should call validation function", () => {
    const validator = mockedValidator;
    const wrapper = getShallowWrapper();

    wrapper.instance().handleValidation(...mockedField);

    expect(validator).toHaveBeenCalled();
  });

  it("should validate the validation function", () => {
    const mockValidator = jest.fn(() => true);
    const wrapper = getShallowWrapper(mockValidator);

    const isValid = wrapper.instance().handleValidation(...mockedField);

    expect(isValid).toBeTruthy();
  });

  it("should not validate the validation function", () => {
    const mockValidator = jest.fn(() => false);
    const wrapper = getShallowWrapper(mockValidator);

    const isValid = wrapper.instance().handleValidation(...mockedField);
    const mockedFieldErrorState = wrapper.state().errors["mockedField"];

    expect(mockedFieldErrorState).toEqual(mockedFieldValidationError);
    expect(isValid).toBeFalsy();
  });

  it("should delete errors on input change", () => {
    const wrapper = getShallowWrapper();

    wrapper.setState({ errors: { mockedField: mockedFieldValidationError } });
    wrapper.instance().handleChange(...mockedField);

    expect(wrapper.state().errors["mockedField"]).toBeUndefined();
  });
});
