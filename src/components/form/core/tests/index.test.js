import withFormValidations from "../withFormValidations";

describe("Export withFormValidations", () => {
  it("should export the withFormValidations component", () => {
    expect(withFormValidations).toBeDefined();
    expect(typeof withFormValidations).toBe("function");
  });
});
