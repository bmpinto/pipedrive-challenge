import React from "react";

// utils
import PropTypes from "prop-types";

const Form = ({ children, onSubmit, submitRef }) => {
  const handleSubmit = e => {
    e.preventDefault();

    onSubmit && onSubmit(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      {children}

      <button ref={submitRef} hidden type="submit" />
    </form>
  );
};

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  submitRef: PropTypes.object.isRequired
};

export default Form;
