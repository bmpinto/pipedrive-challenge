import React from "react";
import { shallow, mount } from "enzyme";
import Form from "../Form";

const baseProps = {
  children: <input type="text" />,
  onSubmit: jest.fn(),
  submitRef: React.createRef()
};

const getShallowWrapper = (props = {}) =>
  shallow(<Form {...baseProps} {...props} />);

const getMountWrapper = (props = {}) =>
  mount(<Form {...baseProps} {...props} />);

describe("<Form />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should call onSubmit", () => {
    const wrapper = getMountWrapper();
    const button = wrapper.find("button");

    button.simulate("submit");

    expect(baseProps.onSubmit).toHaveBeenCalled();
  });
});
