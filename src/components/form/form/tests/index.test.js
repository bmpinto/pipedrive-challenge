import Form from "../Form";

describe("Export Form", () => {
  it("should export the Form component", () => {
    expect(Form).toBeDefined();
    expect(typeof Form).toBe("function");
  });
});
