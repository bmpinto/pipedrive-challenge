import React from "react";
import { shallow } from "enzyme";
import VisuallyHidden from "../VisuallyHidden";

const baseProps = {
  children: "Custom text"
};

const getShallowWrapper = (props = {}) =>
  shallow(<VisuallyHidden {...baseProps} {...props} />);

describe("<VisuallyHidden />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });
});
