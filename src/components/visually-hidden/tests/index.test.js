import VisuallyHidden from "../VisuallyHidden";

describe("Export VisuallyHidden", () => {
  it("should export the VisuallyHidden component", () => {
    expect(VisuallyHidden).toBeDefined();
    expect(typeof VisuallyHidden).toBe("function");
  });
});
