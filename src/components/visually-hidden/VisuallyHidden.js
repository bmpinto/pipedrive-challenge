import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// styles
import styles from "./VisuallyHidden.module.scss";

const VisuallyHidden = ({ children, as, className, ...remainingProps }) => {
  const Component = as ? as : "span";

  return (
    <Component
      {...remainingProps}
      className={classNames(styles.visuallyHidden, className)}
    >
      {children}
    </Component>
  );
};

VisuallyHidden.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  as: PropTypes.string,
  className: PropTypes.string
};

export default VisuallyHidden;
