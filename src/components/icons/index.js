import { ReactComponent as IconCross } from "media/icons/icon-cross.svg";
import { ReactComponent as IconSearch } from "media/icons/icon-search.svg";
import { ReactComponent as IconOrganization } from "media/icons/icon-organization.svg";

export { IconCross, IconSearch, IconOrganization };
