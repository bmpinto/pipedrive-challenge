import React from "react";
import { shallow } from "enzyme";
import Modal from "../Modal";

const baseProps = {
  children: <span>Custom text</span>,
  isActive: true
};

const getShallowWrapper = (props = {}) =>
  shallow(<Modal {...baseProps} {...props} />);

describe("<Modal />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName",
      overlayClassName: "customOverlayClassName"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
