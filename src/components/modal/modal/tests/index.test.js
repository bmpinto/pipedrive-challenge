import Modal from "../Modal";

describe("Export Modal", () => {
  it("should export the Modal component", () => {
    expect(Modal).toBeDefined();
    expect(typeof Modal).toBe("function");
  });
});
