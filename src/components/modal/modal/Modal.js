import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import ReactModal from "react-modal";
import styles from "./Modal.module.scss";

class Modal extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    isActive: PropTypes.bool.isRequired,
    className: PropTypes.string,
    overlayClassName: PropTypes.string,
    onDismiss: PropTypes.func
  };

  render() {
    const {
      children,
      isActive,
      onDismiss,
      className,
      overlayClassName
    } = this.props;

    const appElement = document && document.querySelector("#root");

    return (
      <ReactModal
        appElement={appElement}
        onRequestClose={() => onDismiss && onDismiss()}
        isOpen={isActive}
        className={classNames(styles.overlayContent, className)}
        overlayClassName={classNames(styles.overlay, overlayClassName)}
        shouldCloseOnOverlayClick
      >
        {children}
      </ReactModal>
    );
  }
}

export default Modal;
