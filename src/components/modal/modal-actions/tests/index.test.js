import ModalActions from "../ModalActions";

describe("Export ModalActions", () => {
  it("should export the ModalActions component", () => {
    expect(ModalActions).toBeDefined();
    expect(typeof ModalActions).toBe("function");
  });
});
