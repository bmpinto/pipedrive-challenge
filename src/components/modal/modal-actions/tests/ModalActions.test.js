import React from "react";
import { shallow } from "enzyme";
import ModalActions from "../ModalActions";

const baseProps = {
  onCloseButtonClick: jest.fn()
};

const getShallowWrapper = (props = {}) =>
  shallow(<ModalActions {...baseProps} {...props} />);

describe("<ModalActions />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper(baseProps.onCloseButtonClick)).toMatchSnapshot();
  });

  it("should render call onCloseButtonClick", () => {
    const wrapper = getShallowWrapper(baseProps.onCloseButtonClick);

    wrapper
      .find("Button")
      .last()
      .simulate("click");

    expect(baseProps.onCloseButtonClick).toHaveBeenCalled();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName",
      children: <span />
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
