import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// components
import { Button } from "components/button";

// styles
import styles from "./ModalActions.module.scss";
import sharedStyles from "../shared/ModalShared.module.scss";

const ModalActions = ({ children, className, onCloseButtonClick }) => {
  return (
    <div className={classNames(sharedStyles.padded, styles.actions, className)}>
      {children}

      <Button onClick={() => onCloseButtonClick && onCloseButtonClick()}>
        Back
      </Button>
    </div>
  );
};

ModalActions.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onCloseButtonClick: PropTypes.func.isRequired
};

export default ModalActions;
