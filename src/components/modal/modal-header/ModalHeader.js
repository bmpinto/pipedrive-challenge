import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// components
import { Button, VisuallyHidden } from "components";

// styles
import styles from "./ModalHeader.module.scss";
import sharedStyles from "../shared/ModalShared.module.scss";
import { IconCross } from "components/icons";

const ModalHeader = ({ className, onCloseButtonClick, title }) => {
  return (
    <div className={classNames(sharedStyles.padded, styles.header, className)}>
      <h2 className={styles.title}>{title}</h2>
      <Button
        className={styles.iconButton}
        onClick={() => onCloseButtonClick && onCloseButtonClick()}
      >
        <VisuallyHidden>Close</VisuallyHidden>
        <IconCross className={styles.icon} />
      </Button>
    </div>
  );
};

ModalHeader.propTypes = {
  className: PropTypes.string,
  onCloseButtonClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default ModalHeader;
