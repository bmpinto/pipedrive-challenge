import React from "react";
import { shallow } from "enzyme";
import ModalHeader from "../ModalHeader";

const baseProps = {
  onCloseButtonClick: jest.fn(),
  title: "Title"
};

const getShallowWrapper = (props = {}) =>
  shallow(<ModalHeader {...baseProps} {...props} />);

describe("<ModalHeader />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper(baseProps.onCloseButtonClick)).toMatchSnapshot();
  });

  it("should render call onCloseButtonClick", () => {
    const wrapper = getShallowWrapper(baseProps.onCloseButtonClick);

    wrapper.find("Button").simulate("click");

    expect(baseProps.onCloseButtonClick).toHaveBeenCalled();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName",
      children: <span />
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
