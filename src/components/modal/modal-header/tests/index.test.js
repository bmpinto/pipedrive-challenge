import ModalHeader from "../ModalHeader";

describe("Export ModalHeader", () => {
  it("should export the ModalHeader component", () => {
    expect(ModalHeader).toBeDefined();
    expect(typeof ModalHeader).toBe("function");
  });
});
