import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// styles
import sharedStyles from "../shared/ModalShared.module.scss";

const ModalContent = ({ children, className }) => (
  <div className={classNames(sharedStyles.padded, className)}>{children}</div>
);

ModalContent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

export default ModalContent;
