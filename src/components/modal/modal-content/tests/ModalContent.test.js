import React from "react";
import { shallow } from "enzyme";
import ModalContent from "../ModalContent";

const baseProps = {
  children: <span />
};

const getShallowWrapper = (props = {}) =>
  shallow(<ModalContent {...baseProps} {...props} />);

describe("<ModalContent />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });

  it("should render correctly with custom props", () => {
    const props = {
      className: "customClassName"
    };

    expect(getShallowWrapper(props)).toMatchSnapshot();
  });
});
