import ModalContent from "../ModalContent";

describe("Export ModalContent", () => {
  it("should export the ModalContent component", () => {
    expect(ModalContent).toBeDefined();
    expect(typeof ModalContent).toBe("function");
  });
});
