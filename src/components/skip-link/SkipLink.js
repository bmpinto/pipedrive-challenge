import React from "react";

// utils
import PropTypes from "prop-types";
import classNames from "classnames";

// components
import { VisuallyHidden } from "components";

// styles
import styles from "./SkipLink.module.scss";

const SkipLink = ({ children, className, to }) => (
  <VisuallyHidden
    as="a"
    href={`#${to}`}
    className={classNames(styles.skipLink, className)}
  >
    {children}
  </VisuallyHidden>
);

SkipLink.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  className: PropTypes.string,
  to: PropTypes.string.isRequired
};

export default SkipLink;
