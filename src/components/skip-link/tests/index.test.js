import SkipLink from "../SkipLink";

describe("Export SkipLink", () => {
  it("should export the SkipLink component", () => {
    expect(SkipLink).toBeDefined();
    expect(typeof SkipLink).toBe("function");
  });
});
