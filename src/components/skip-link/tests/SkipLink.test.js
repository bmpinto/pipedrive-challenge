import React from "react";
import { shallow } from "enzyme";
import SkipLink from "../SkipLink";

const baseProps = {
  children: "Custom text",
  to: "main"
};

const getShallowWrapper = (props = {}) =>
  shallow(<SkipLink {...baseProps} {...props} />);

describe("<SkipLink />", () => {
  it("should render correctly", () => {
    expect(getShallowWrapper()).toMatchSnapshot();
  });
});
