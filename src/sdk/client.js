import axios from "axios";
import httpAdapter from "axios/lib/adapters/http";
import idx from "idx.macro";

export default config => {
  if (!config) {
    throw new Error("Invalid config");
  }

  if (!config.apiURL) {
    throw new Error("Invalid API URL");
  }

  if (!config.apiToken) {
    throw new Error("Invalid API Token");
  }

  // we need to enforce the node adapter in order to mock http requests.
  // https://github.com/nock/nock/issues/699#issuecomment-272708264
  axios.defaults.adapter = httpAdapter;

  const axiosClient = axios.create({
    baseURL: config.apiURL,
    params: {
      api_token: config.apiToken
    }
  });

  let client = {};

  // Wraps axios client in order to always return the "data" object
  ["request", "get", "delete", "head", "options", "post", "put", "patch"].map(
    method =>
      (client[method] = (...args) =>
        axiosClient[method](...args)
          .then(response => response && response.data)
          .catch(error => idx(error, _ => _.response.data)))
  );

  return client;
};
