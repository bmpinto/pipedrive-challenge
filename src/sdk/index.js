import client from "./client";
import config from "./config";
import modules from "./modules";
import adapters from "./adapters";

export const configuredClient = client(config);
export const configuredAdapters = adapters(config);

export default {
  ...modules(configuredClient, configuredAdapters)
};
