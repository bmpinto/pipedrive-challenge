import persons from "./persons";

export default (client, adapters) => ({
  persons: persons(client, adapters)
});
