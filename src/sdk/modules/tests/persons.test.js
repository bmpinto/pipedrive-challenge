import persons from "../persons";
import { PERSONS_ENDPOINT } from "../persons";

const mocked = {
  data: "mockedData",
  response: "mockedResponse",
  body: "mockedBody"
};

const mockedClient = {
  get: jest.fn(() => Promise.resolve({})),
  post: jest.fn(() => Promise.resolve({})),
  delete: jest.fn(() => Promise.resolve({}))
};

const mockedAdapters = {
  persons: {
    get: {
      toStandardResponse: () => mocked.response
    },
    add: {
      toStandardResponse: () => mocked.response,
      toStandardBody: () => mocked.body
    },
    remove: {
      toStandardResponse: () => mocked.response,
      toStandardBody: () => mocked.body
    }
  }
};

describe("Persons modules", () => {
  it("should call client get() with correct endpoint and adapt response ", async () => {
    const result = await persons(mockedClient, mockedAdapters).get();

    expect(mockedClient.get).toHaveBeenCalled();
    expect(mockedClient.get).toHaveBeenCalledWith(PERSONS_ENDPOINT);
    expect(result).toBe(mocked.response);
  });

  it("should call client add() with correct endpoint and adapt response ", async () => {
    const result = await persons(mockedClient, mockedAdapters).add(mocked.data);

    expect(mockedClient.post).toHaveBeenCalled();
    expect(mockedClient.post).toHaveBeenCalledWith(
      PERSONS_ENDPOINT,
      mocked.body
    );
    expect(result).toBe(mocked.response);
  });

  it("should call client remove() with correct endpoint and adapt response ", async () => {
    const result = await persons(mockedClient, mockedAdapters).remove(
      mocked.data
    );

    expect(mockedClient.delete).toHaveBeenCalled();
    expect(mockedClient.delete).toHaveBeenCalledWith(
      `${PERSONS_ENDPOINT}/${mocked.data}`
    );
    expect(result).toBe(mocked.response);
  });
});
