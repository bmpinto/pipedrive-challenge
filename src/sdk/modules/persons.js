import urlJoin from "proper-url-join";

export const PERSONS_ENDPOINT = "/persons";

export const get = (client, adapters) => {
  return client
    .get(PERSONS_ENDPOINT)
    .then(adapters.persons.get.toStandardResponse);
};

export const add = (client, adapters, data) => {
  return client
    .post(PERSONS_ENDPOINT, adapters.persons.add.toStandardBody(data))
    .then(adapters.persons.add.toStandardResponse);
};

export const remove = (client, adapters, id) => {
  const url = urlJoin(PERSONS_ENDPOINT, id);

  return client.delete(url).then(adapters.persons.remove.toStandardResponse);
};

export default (client, adapters) => ({
  get: () => get(client, adapters),
  add: name => add(client, adapters, name),
  remove: id => remove(client, adapters, id)
});
