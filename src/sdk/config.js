export default {
  apiURL: process.env.REACT_APP_API_URL,
  apiToken: process.env.REACT_APP_API_TOKEN,
  customFields: {
    groups: process.env.REACT_APP_CUSTOM_FIELD_PERSON_GROUPS,
    assistant: process.env.REACT_APP_CUSTOM_FIELD_PERSON_ASSISTANT,
    location: process.env.REACT_APP_CUSTOM_FIELD_PERSON_LOCATION
  }
};
