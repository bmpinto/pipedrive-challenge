import persons from "./persons";

export default config => ({
  persons: persons(config)
});
