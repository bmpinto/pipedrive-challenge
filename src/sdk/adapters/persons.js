import idx from "idx.macro";
import pickBy from "lodash.pickby";
import identity from "lodash.identity";

const pictureSizes = {
  small: "128",
  large: "512"
};

const adaptPicture = person => {
  const pictureId = idx(person, _ => _.picture_id.pictures[pictureSizes.small]);
  const pictureUrl = idx(person, _ => _.picture.url);

  return pictureId || pictureUrl || null;
};

const adaptPhone = phone => (Array.isArray(phone) ? phone[0].value : phone);

const adaptEmail = email => (Array.isArray(email) ? email[0].value : email);

const adaptPerson = (config, person) => {
  const { assistant, groups, location } = config.customFields;
  const picture = adaptPicture(person);
  const phone = adaptPhone(person.phone);
  const email = adaptEmail(person.email);

  const adaptedPerson = {
    assistant: person[assistant],
    groups: person[groups],
    location: person[location],
    email: email,
    id: person.id,
    name: person.name,
    organization: person.org_name,
    phone: phone,
    picture: picture
  };

  return pickBy(adaptedPerson, identity);
};

const toStandardBody = (config, person) => {
  const { assistant, groups, location } = config.customFields;

  return {
    [assistant]: person.assistant,
    [groups]: person.groups,
    [location]: person.location,
    email: person.email,
    name: person.name,
    phone: person.phone,
    org_id: 1
  };
};

export const get = (config, response) => ({
  ...response,
  data:
    response.data && response.data.map(person => adaptPerson(config, person))
});

export const getPerson = (config, response) => ({
  ...response,
  data: response.data && adaptPerson(config, response.data)
});

export default config => ({
  get: {
    toStandardResponse: response => get(config, response)
  },
  add: {
    toStandardResponse: response => getPerson(config, response),
    toStandardBody: response => toStandardBody(config, response)
  },
  remove: {
    toStandardResponse: response => getPerson(config, response),
    toStandardBody: response => toStandardBody(config, response)
  }
});
