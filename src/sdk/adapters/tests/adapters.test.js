import adapters from "../";
import config from "../../config";
import {
  mockedPerson,
  mockAdaptedPerson,
  mockedPersons,
  mockedPersonToStandardBody
} from "utils/mocks";

describe("Person adapters", () => {
  const { persons } = adapters(config);

  describe("get.toStandardResponse()", () => {
    it("should return the adapted response", () => {
      const adaptedResponse = persons.get.toStandardResponse(mockedPersons);

      expect(adaptedResponse).toEqual({ data: [mockAdaptedPerson] });
    });
  });

  describe("Add person adapters", () => {
    describe("add.toStandardResponse()", () => {
      it("should return the adapted response", () => {
        const adaptedResponse = persons.add.toStandardResponse(mockedPerson);

        expect(adaptedResponse).toEqual({ data: mockAdaptedPerson });
      });
    });

    describe("add.toStandardBody()", () => {
      it("should return the adapted response", () => {
        const adaptedResponse = persons.add.toStandardBody(mockAdaptedPerson);

        expect(adaptedResponse).toEqual(mockedPersonToStandardBody);
      });
    });
  });

  describe("Remove person adapters", () => {
    describe("remove.toStandardResponse()", () => {
      it("should return the adapted response", () => {
        const adaptedResponse = persons.remove.toStandardResponse(mockedPerson);

        expect(adaptedResponse).toEqual({ data: mockAdaptedPerson });
      });
    });

    describe("remove.toStandardBody()", () => {
      it("should return the adapted response", () => {
        const adaptedResponse = persons.remove.toStandardBody(
          mockAdaptedPerson
        );

        expect(adaptedResponse).toEqual(mockedPersonToStandardBody);
      });
    });
  });
});
