import client from "../client";
import nock from "nock";

const mockedConfig = {
  apiURL: "https://localhost:3000",
  apiToken: "token"
};

const configuredClient = client(mockedConfig);

describe("utils client", () => {
  it("should throw an error if config is not provided", () => {
    expect(() => client()).toThrow("Invalid config");
  });

  it("should throw an error if the given apiURL is not provided", () => {
    expect(() => client({})).toThrow("Invalid API URL");
  });

  it("should throw an error if the given apiToken is not provided", () => {
    expect(() => client({ apiURL: mockedConfig.apiURL })).toThrow(
      "Invalid API Token"
    );
  });

  it("should use get() method and return success", async () => {
    const mockedResponse = "test data";

    nock(mockedConfig.apiURL)
      .get("/")
      .query({ api_token: mockedConfig.apiToken })
      .reply(200, mockedResponse);

    const result = await configuredClient.get("/");

    expect(result).toBe(mockedResponse);
  });

  it("should have get method", () => {
    expect(configuredClient.get).toBeDefined();
    expect(() => configuredClient.get()).not.toThrow();
  });

  it("should have post method", () => {
    expect(configuredClient.post).toBeDefined();
    expect(() => configuredClient.post()).not.toThrow();
  });

  it("should have delete method", () => {
    expect(configuredClient.delete).toBeDefined();
    expect(() => configuredClient.delete()).not.toThrow();
  });

  it("should have put method", () => {
    expect(configuredClient.put).toBeDefined();
    expect(() => configuredClient.put()).not.toThrow();
  });

  it("should have patch method", () => {
    expect(configuredClient.patch).toBeDefined();
    expect(() => configuredClient.patch()).not.toThrow();
  });

  it("should have request method", () => {
    expect(configuredClient.request).toBeDefined();
    expect(() => configuredClient.request()).not.toThrow();
  });

  it("should have head method", () => {
    expect(configuredClient.head).toBeDefined();
    expect(() => configuredClient.head()).not.toThrow();
  });

  it("should have options method", () => {
    expect(configuredClient.options).toBeDefined();
    expect(() => configuredClient.options()).not.toThrow();
  });
});
