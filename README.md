# Pipedrive's Front-End Developer Asssignment

When​ ​a​ ​user​ ​visits​ ​the​ ​page,​ ​a​ ​list​ ​of​ ​Persons​ ​fetched​ ​from​ ​Pipedrive​ ​API​ ​is​ ​shown.​ ​When​ ​the user​ ​clicks​ ​in​ ​one​ ​of​ ​the​ ​Persons​ ​in​ ​the​ ​list,​ ​a​ ​modal​ ​is​ ​shown​ ​with​ ​that​ ​Person’s​ ​additional details.​ ​The​ ​user​ ​is​ ​also​ ​able​ ​to​ ​order​ ​each​ ​Person​ ​in​ ​the​ ​list​ ​via​ ​drag-and-drop.

![Projects screenshot](./screenshot.png?raw=true)

## Getting Started

In the project directory, you can run these commands:

```
// install dependencies
npm install

// start project
npm start

// test project
npm run test

// build project
npm run build

// run the build locally
npm install -g serve
serve -s build
```

## Disclaimer

There's an `.env` file that includes the company domain URL and its token.

Just to be clear: this was only included to facilitate project's bootstrapping.
If this was a real project the `.env` file should've been listed in `.gitignore`.
Then again, we could still provide - _like we do_ - an `.env.sample` with the recommended `.env` structure.

It's worth to mention that if we change every key in the `.env` to another valid company info, we can run this challenge against any domain data.

## Tooling

These were the tools used in the challenge:

- React
- CSS Modules
- Jest
- Axios
- Prettier

The first three listed tools had some level of pre-configuration provided by [Create React App](https://facebook.github.io/create-react-app/)

## Features

1. Get list of persons from the API
2. Show additional person details
3. _[Bonus]_ Add person using the API
4. _[Bonus]_ Remove person using the API
5. _[Bonus]_ Search for a person
6. _[Bonus²]_ An easily extensible and fully tested SDK to interact with the API

## Missing Features

1. Store order of persons
2. Paginated persons list

## Mindset and Quick Wins

Make sure all I did was accessible by default. This mindset not only benefits those with disabilities but also gives us an improvement in user experience. I consider these basic requirements for a project since with little effort we can make such a big difference. Quick wins!

## Components' Approach

There was also a primary concern with component reusability and testing. Regarding the latter, there is currently a total of more than 95 unit tests in the project. On the subject of reusability, I'm pretty happy with the end result, mainly with both high order components:

#### withFormValidations

Easy way to provide validations to any Form. We just need to pass field validators to it and they're injected in any component we'd like.

#### withPersonsController

Anything regarding persons' actions is provided by this component. this gives us a great level of extensibility since we could use it in any place we need to add, remove or get a person.

Other than those, I guess almost every other component follow the same reusability approach.

- Button
- SkipLink
- VisuallyHidden
- Form
- FormField
- Modal
- ContentWrapper
- Loader
- PersonAvatar
- ...

## Extra Mile

As a bonus I've decided to create an SDK to make it easy to extend to any part of the API. In this challenge we just make great use of Persons but we could easily extend to anything else like Deals, Organizations, etc.

## Known Issues

There's some more work to be done on loading state indicators and error handling.
Currently we can add a person successfully, and I've spend quite some time with this issue, but that newly added person isn't showing up when we make a request to get all persons.
